# Processing Events

Right, you have your bot up and running and now you want to make it actually do stuff. To do so we got to understand how the Discord API talks with connected clients. Basically, there're two ways information is transferred between you bot and Discord - gateway events and the RESTful API. The former one is how Discord sends you new stuff that is happening, such as new messages or a new member joining a guild; the latter is how you send stuff over to Discord. In this section I'll be covering how you can handle the gateway events using `discord.py`.  

There are two ways you can register a function to be run when a new event comes in and they come in the form as decorators you put over the function you want to register:
- `bot.event` -> This decorator will override every previous occurance of handlers for the specified event, which you specify by naming the function using this format: `on_<event name>`. You can see the full list of available events [**here**](https://rapptz.github.io/discord.py/docs/api.html#event-reference).
- `bot.listen` -> This is a decorator generator that can take an event name and will register the decorated function as one to be executed when the specified event comes in. This allows you to split your event logic into smaller pieces and name your functions something more meaningful. If no name is passed in the decorator generator, then the library will try to guess the event name just like in `bot.event`. This method is the preferred way of registering event handlers but for some unholy reason it's not available in the `discord.Client` class...

*Note: All event handlers should be coroutine functions (`async def`)!*  

---

Let's see some example code!  

`main.py`
```python
from discord.ext.commands import Bot
from textwrap import dedent  # Just so our code doesn't look bad

with open("path/to/token.txt") as fp:
    TOKEN = fp.read().strip()

bot = Bot(command_prefix="bot!")

@bot.listen("on_start")
async def on_start_print_bot_info():
    """Prints out some info about the bot once it's ready to go"""
    owner = (await bot.application_info()).owner

    print(dedent(
        f"""
        I'm ready to go!
        Logged in as: {bot.user}
        User ID: {bot.user.id}
        Owner: {owner}
        """
        ))

@bot.listen("on_message")
async def on_mention_reply_prefix(message: discord.Message) -> None:
    """Replies the bot's prefix when mentioned"""
    if bot.user.mentioned_in(message):
        await message.channel.send(f"**Hiya! My prefix is `{bot.command_prefix}`.**")

# Never forget to leave this on the bottom of your code
# Anything after this line will only be executed once the bot logs out
bot.run(TOKEN)
```

---

Now that you have mastered the handling of events, it's time to move forward into commands! Head over to [**Adding Commands**](./adding-commands.html) and I'll explain to you how you can make cool commands for your bot!
