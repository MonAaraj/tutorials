# Summary

[Introduction](./introduction.md)

---

- [Starting](./starting/introduction.md)
    - [Programming](./starting/programming.md)
    - [Making the Bot App](./starting/making-the-bot.md)
    - [Reading the Docs](./starting/reading-the-docs.md)

- [FAQs](./faqs/introduction.md)
    - [Token Leaked](./faqs/token-leaked.md)
    - [Hosting with Glitch](./faqs/glitch-hosting.md)
    - [Installing Python](./faqs/installing-python.md)
    - [Python Venvs](./faqs/python-venvs.md)
    - [Environment Variables](./faqs/env-vars.md)
    - [API Collection](./faqs/apis.md)

- [Discord Wrappers](./wrappers.md)
    - [discord.py](./dpy/introduction.md)
        - [Getting Started](./dpy/getting-started.md)
        - [Processing Events](./dpy/processing-events.md)
        - [Adding Commands](./dpy/adding-commands.md)
        - [Command Arguments](./dpy/command-args.md)
        - [Checks and Cooldowns](./dpy/checks-cooldowns.md)
        - [Command Groups](./dpy/command-groups.md)
        - [Error Handling](./dpy/error-handling.md)
        - [Extensions](./dpy/extensions.md)
        - [Useful Need-To-Knows](./dpy/need-to-knows.md)
    - [JDA](./jda/introduction.md)
        - [Getting Started](./jda/getting-started.md)
        - [Coding The Bot](./jda/coding-the-bot.md)
        - [Commands, Listeners and Activities](./jda/commands-listeners-and-activities.md)
        - [Embeds](./jda/embeds.md)
        - [ReadyEvent and Logging](./jda/readyevent-and-logging.md)
        - [Reading Errors](./jda/reading-errors.md)

- [Production Environments & Databases](./env-db.md)
    - [Docker](./docker/introduction.md)
        - [Installing](./docker/installing.md)
        - [Running a Python Bot](./docker/pythonbot.md)
        - [Running your Python Bot](./docker/yourbotpy.md)
        - [Docker Compose](./docker/compose.md)
        - [Database on Docker](./docker/database.md)
    - [MongoDB](./mongodb/introduction.md)
        - [Atlas Cluster Setup](./mongodb/atlas-setup.md)
        - [Starting to Code - Python](./mongodb/start-py-coding.md)
        - [Main Methods - Python](./mongodb/main-py-methods.md)
    - [Redis](./redis/introduction.md)
        - [Installing](./redis/installing.md)
        - [Setting up the server](./redis/setting-up-the-server.md)
        - [Starting with aioredis](./redis/starting-with-aioredis.md)
        - [Final Notes](./redis/final-notes.md)

---

[Contributors](./contributors.md)
